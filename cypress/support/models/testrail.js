/// <reference types="cypress" />

export class TestRail{
    addCase(title){ 
        
        cy.request({
            url: Cypress.env('testrailEndpoint') + '/index.php?/api/v2/add_case/3',
            method: 'POST',
            headers: { 
                "Authorization": "Basic cHJlbWUud0BmaWxsZ29vZHMuY286SEBwcHl3QHJrNjg="
            },
            body: { 
                title: 	title,
                template_id	: '',
                type_id: '',
                priority_id: '',
                estimate: '30s',
                milestone_id: 6,
                refs: ''
            }
        })
        
    }
}

export const mTestRail = new TestRail; 

