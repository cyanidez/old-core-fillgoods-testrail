// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import { onDashboardPage } from "./pageObjects/coreFillgoods/dashboard/dashboardPage";
import { onLoginpage } from "./pageObjects/coreFillgoods/login/loginPage";
import { onBeamerPage } from "./pageObjects/coreFillgoods/modal/beamerPage";
import { onTutorialPage } from "./pageObjects/coreFillgoods/modal/tutorialPage";
import { onLoginOmsPage, onLoginPage } from "./pageObjects/oms/login/loginOmsPage";

Cypress.Commands.add('authenFirebase', () => {
    cy.log('=========');
    cy.request({
        url: Cypress.env('firebaseEndpoint') + '/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDGyhsoYSfoQdxuDe8_u0if4xgzqLWK0XY&email=test0716124844@test.com&password=123456&returnSecureToken=true',
        method: "POST",
        headers: { 
            "content-type": "application/json"
        }
    }).its('body').then(respBody => {
        cy.wrap(respBody).as('respFirebase');
    })
    
})

Cypress.Commands.add('openCoreFillgoods', () => {
    cy.visit('/');
})
Cypress.Commands.add('openCoreFillgoodsOldDesign', () => {
    cy.visit(Cypress.env('loginUrl'));
})

Cypress.Commands.add('openOms', () => {
    cy.visit(Cypress.env('omsUrl'));
    
})
Cypress.Commands.add('loginOMS', (email, password, mode = 'uat') => {
    cy.openOms(); 
    onLoginOmsPage.fillEmail(email);
    onLoginOmsPage.fillPassword(password);
    onLoginOmsPage.selectMode(mode);
    onLoginOmsPage.clickLoginButton();
    
})

Cypress.Commands.add('loginToCoreFillgoods', (bypassTutorial = true) => {
    let user; 
    let pass; 
    cy.fixture('testData/login.json').then(loginData => {
        cy.log("loginData = " + JSON.stringify(loginData))
        user = loginData.validCredential.user; 
        pass = loginData.validCredential.pass; 
        
        // cy.openCoreFillgoodsOldDesign();
        cy.openCoreFillgoods();
        
        onLoginpage.fillEmail(user);
        onLoginpage.fillPassword(pass);
        onLoginpage.clickLoginButton();
        onDashboardPage.pageShouldBeDisplayed()
        onBeamerPage.closeBeamer();

        if (bypassTutorial == true){
            onTutorialPage.tutorialShouldBeDisplayed();
            onTutorialPage.closeTutorial();
        }
    })
    
    
    
    // o nLoginpage.fillPassword(user);
    // onLoginpage.fillPassword(pass);
    // onLoginpage.clickLoginButton();
    // onDashboardPage.pageShouldBeDisplayed()
    // onBeamerPage.closeBeamer();

    // if (bypassTutorial == true){
    //     onTutorialPage.tutorialShouldBeDisplayed();
    //     onTutorialPage.closeTutorial();
    // }
})

