/// <reference types="cypress" />

export class APIHelper {
    setReqUri(reqUri){
        cy.wrap(reqUri).as('reqUri');
    }
    setReqHeader(reqHeaders){
        cy.wrap(reqHeaders).as('reqHeaders');
    }
    setReqBody(reqbody){
        cy.wrap(reqbody).as('reqBody');
    }

    

    setReqMethod(reqMethod){
        cy.wrap(reqMethod).as('reqMethod');
    }

    setReqCustomBody(data, key, value){
        data.key = value; 
    }
    // setRespStatusCode(statusCode){
    //     let respStatusCode = statusCode; 
    //     cy.wrap(respStatusCode).as('respStatusCode');
    // }

    // setRespBody(body){
    //     let respBody = body; 
    //     cy.wrap(respBody).as('respBody');
    // }

    sendRequest(alias){
        
        let requestData = {}
        
        cy.get('@reqUri').then(reqUri => {
            requestData.url = reqUri; 
        });
        cy.get('@reqMethod').then(reqMethod => {
            requestData.method = reqMethod; 
        })
        cy.get('@reqBody').then(reqBody => {
            requestData.body = reqBody; 
        })
        cy.get('@reqHeaders').then(reqHeaders => {
            requestData.headers = reqHeaders; 
        })
        cy.request(requestData).then(response => {
            cy.wrap(response).as(alias);
        });

    }

    

    // getBody(alias){
    //     let body = cy.get('@' + alias)
    //     cy.log(body);
    // }

    // getStatusCode(alias){
    //     if (alias == "")
    //         return null;
    //     cy.get('@' + alias).then({

    //     })
    // }
}

export const apiHelper = new APIHelper; 