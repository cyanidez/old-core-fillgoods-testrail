/// <reference types="cypress" />

const pageLocator = { 
    txtEmail: '[placeholder="Username"]',
    txtPassword: '[placeholder="Password"]',
    ddlMode: '[placeholder="Select"]'
}

export class SidebarPage { 
    expandMenu(){
        cy.get('.side-menu-vertical-top li div').eq(0).click();
        cy.wait(1000);
    }

    
    
    clickMenu(mainMenuName){
        cy.log("mainMenuName = " + mainMenuName);
        // cy.get('.side-menu-vertical-top li');
        cy.contains(mainMenuName).click(); 
        // cy.contains('.side-menu-vertical-top li div', mainMenuName).click(); 
    }
    clickSubMenu(mainMenuName, subMenuName){
        cy.contains('.side-menu-vertical-top li', mainMenuName)
            .find('ul li').contains(subMenuName);
    }
}

export const onSidebarPage = new SidebarPage; 