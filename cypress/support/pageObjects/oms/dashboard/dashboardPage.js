/// <reference types="cypress" />

export class DashboardPage { 
    dashboardShouldBeDisplay(){
        cy.url().should('contain', 'oms/home');
    }
}

export const onDashboardPage = new DashboardPage; 