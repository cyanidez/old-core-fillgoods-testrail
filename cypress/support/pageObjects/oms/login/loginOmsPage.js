/// <reference types="cypress" />

const pageLocator = { 
    txtEmail: '[placeholder="Username"]',
    txtPassword: '[placeholder="Password"]',
    ddlMode: '[placeholder="Select"]'
}

export class LoginOmsPage { 
    fillEmail(email){
        cy.get(pageLocator.txtEmail)
            .clear()
            .type(email)
            .should('have.value', email);
    }
    fillPassword(password){
        cy.get(pageLocator.txtPassword)
            .clear()
            .type(password)
            .should('have.value', password);
    }

    selectMode(modeName){
        cy.get(pageLocator.ddlMode).click(); 
        cy.contains('.el-select-dropdown__list li span', modeName).click();
    }

    clickLoginButton(){
        cy.intercept('POST', '**/verifyPassword**').as('verifyPassword');
        cy.contains('button', 'เข้าสู่ระบบ').click();
        cy.wait('@verifyPassword');
        cy.wait(6000);
    }
}

export const onLoginOmsPage = new LoginOmsPage; 