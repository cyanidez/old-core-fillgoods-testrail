/// <reference types="cypress" />

export class TopMenuPage { 
    
    clickMainMenu(label){
        cy.intercept('GET', '**/get-cache**').as('getCache');
        cy.contains('.v-btn', label).click()
    }

    clickSubMenu(label){
        cy.contains('.v-list__tile--link', label).click()
    }
    
}

export const onTopMenuPage = new TopMenuPage; 

