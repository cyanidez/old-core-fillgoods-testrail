/// <reference types="cypress" />
const pageLocator = { 
    ddlPricePlan : '[data-testid="ddlPricePlan"]'
    
}

export class PricePlanPage { 
    choosePricePlan(label){
        cy.get(pageLocator.ddlPricePlan).select(label);
    }

    chooseTransferType(label){

    }
}

export const onPricePlanPage = new PricePlanPage(); 