/// <reference types="cypress" />

export class TutorialPage { 
    clickTab(){
        
    }

    tutorialShouldBeDisplayed(){
        cy.get('.dialog-wrapper').should('be.visible');
    }
    closeTutorial(){
        cy.get('.dialog-wrapper').find('button.btn-close').click();
    }
}

export const onTutorialPage = new TutorialPage; 