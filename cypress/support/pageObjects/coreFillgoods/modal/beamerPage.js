/// <reference types="cypress" />

export class BeamerPage { 
    closeBeamer(){
        // cy.get('#beamerNews').invoke('attr', 'style', 'display:none');
        // cy.get('#beamerOverlay').invoke('attr', 'style', 'display:none');
        cy.wait(5000);
        cy.get('body').then($body => {
            if ($body.find('#beamerNews').length > 0) {
                cy.get('#beamerNews').invoke('attr', 'style', 'display:none');
                cy.get('#beamerOverlay').invoke('attr', 'style', 'display:none');
            }
        });
    }
}

export const onBeamerPage = new BeamerPage; 

