/// <reference types="cypress" />

export class SurveyPage {
    chooseChannel(channel){
        cy.contains('div.survey-selection-social-cols', channel).click();
    }
    clickNextButton(label){
        cy.contains('button', label).should('be.enabled').click()
    }
    pageShouldBeDisplayed(){
        cy.url().should('include', 'register/survey');
    }

    chooseSalesOrder(salesOrder){
        cy.get('.v-select__selections').click();
        cy.get('.v-select-list').find('.v-list-item__title').contains(salesOrder).click();
    }
}

export const onSurveyPage = new SurveyPage;