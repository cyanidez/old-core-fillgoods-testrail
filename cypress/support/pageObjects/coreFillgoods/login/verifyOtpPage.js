/// <reference types="cypress" />

export class VerifyOtpPage {
    pageShouldBeDisplayed(){
        cy.url().should('include', 'register/otp');
    }
    
    fillOtp(otpCode){
        cy.intercept('POST', '**/user/send-otp').as('sendOtp');
        cy.intercept('POST', '**/user/create-user-and-shop').as('createUserAndShop');
        cy.get('[data-testid="otpInput"]').clear().type(otpCode);
        cy.wait('@sendOtp');
        // cy.wait('@createUserAndShop');
    }

    clickOKButton(){
        cy.contains('button', 'ตกลง').click();
    }
}

export const onVerifyOtpPage = new VerifyOtpPage; 