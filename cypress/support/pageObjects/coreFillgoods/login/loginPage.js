/// <reference types="cypress" />

// const pageLocator = {
//     txtEmail: '[data-testid="emailInput"]',
//     txtPassword: '[data-testid="passwordInput"]'
// }


const pageLocator = {
    txtEmail: ':nth-child(1) > .v-card > :nth-child(1) > .container > .layout > :nth-child(1) > .v-input > .v-input__control > .v-input__slot > .v-text-field__slot > input',
    txtPassword: ':nth-child(1) > .v-card > :nth-child(1) > .container > .layout > :nth-child(2) > .v-input > .v-input__control > .v-input__slot > .v-text-field__slot > input'
}


export class LoginPage{
    clickTab(tabName){
        cy.contains('a[role="tab"]', tabName).click()
    }
    fillEmail(email){
        cy.wait(10000);
        cy.get(pageLocator.txtEmail)
            .type(email);
    }

    fillPassword(password){
        cy.get(pageLocator.txtPassword)
            .type(password);
    }
    clickLoginButton(){
        cy.intercept('POST', '**/relyingparty/verifyPassword**')
            .as('verifyPassword');
        cy.contains('button', 'Log in').click();
        cy.wait('@verifyPassword');
    }
}

export const onLoginpage = new LoginPage