/// <reference types="cypress" />

const pageLocator = {
    txtFirstName: '[data-testid="firstName"]',
    txtLastName: '[data-testid="lastName"]',
    txtPhoneNumber: '[data-testid="phoneNumber"]',
    txtEmail: '[data-testid="email"]',
    txtPassword: '[data-testid="password"]'
    
}
export class RegisterPage { 
    fillFirstName(firstName){
        cy.get(pageLocator.txtFirstName).clear().type(firstName).should('have.value', firstName)

    }
    fillLastName(lastName){
        cy.get(pageLocator.txtLastName).clear().type(lastName).should('have.value', lastName)
    }
    fillPhoneNumber(phoneNumber){
        cy.get(pageLocator.txtPhoneNumber).clear().type(phoneNumber)
    }
    phoneNumberDisplayError(message){
        cy.contains('div.v-input__control', 'เบอร์โทรศัพท์').find('div.v-messages__message').should('contain', message)
    }
    fillEmail(email){
        cy.get(pageLocator.txtEmail).clear().type(email).should('have.value', email)
    }
    emailDisplayError(message){
        cy.contains('div.v-input__control', 'อีเมล').find('div.v-messages__message').should('contain', message)
    }

    fillPassword(password){
        cy.get(pageLocator.txtPassword).clear().type(password).should('have.value', password)
    }
    passwordDisplayError(message){
        cy.contains('div.v-input__control', 'รหัสผ่าน').find('div.v-messages__message').should('contain', message)
    }

    signupButtonShouldBeDisabled(){
        cy.contains('button', 'SIGN UP').should('not.be.enabled')
    }
    clickSignupButton(){
        cy.contains('button', 'SIGN UP').should('be.enabled').click()
    }
    loginDisplayError(message){
        cy.get('div.v-alert__content').should('contain', message)
    }

}

export const onRegisterPage = new RegisterPage