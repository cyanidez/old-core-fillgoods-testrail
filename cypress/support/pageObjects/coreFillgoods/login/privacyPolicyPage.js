/// <reference types="cypress" />

export class PrivacyPolicyPage { 
    tickAcceptPolicy(accept = true){
        cy.get('[data-testid="confirmPolicyCheckbox"]').then(checkboxEl => {
            cy.wrap(checkboxEl).scrollIntoView();

            if (accept == true){
                cy.wrap(checkboxEl).check({force:true}).should('be.checked');
            } else {
                cy.wrap(checkboxEl).uncheck({force:true}).should('not.be.checked');
            }
        })
        
    }
    
    pageShouldBeDisplayed(){
        cy.url().should('include', 'register/privacy-policy');
    }

    clickOKButton(){
        cy.intercept('POST', '**/user/update').as('userUpdate');
        cy.contains('button', 'ตกลง').should('be.enabled').click();
        cy.wait('@userUpdate');
    }

}

export const onPrivacyPolicyPage = new PrivacyPolicyPage;