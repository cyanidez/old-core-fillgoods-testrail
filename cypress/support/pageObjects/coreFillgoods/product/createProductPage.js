const pageLocator = {
    txtProductName: 'input[placeholder="ชื่อสินค้า"]',
    txtProductPrice: 'input[placeholder="0.00"]',
    chkCountStock: 'input[aria-label="นับสต็อก"]'
}

export class createProductPage {
    fillProductName(productName){
        cy.get(pageLocator.txtProductName).clear().type(productName)
            .should('have.value', productName)
    }
    
    fillProductPrice(productPrice){
        cy.get(pageLocator.txtProductPrice).clear().type(productPrice)
            .should('have.value', productPrice)
    }

    fillProductQuantity(quantity){
        cy.get(pageLocator.txtProductQuantity).clear().type(quantity);
    }

    checkCountStock(isCount=true){
        if (isCount == true){
            cy.get(chkCountStock).check({force:true})
        } else {
            cy.get(chkCountStock).uncheck({force:true})
        }
    }
    
    fillWeightGram(weight){
        cy.contains('span', 'น้ำหนัก').eq(0).siblings('div').eq(0).find('input').type(weight)
    }

    fillWeightKilogram(weight){
        cy.contains('span', 'น้ำหนัก').eq(1).siblings('div').eq(0).find('input').type(weight)
    }
    

}

export const onCreateProductPage = new createProductPage