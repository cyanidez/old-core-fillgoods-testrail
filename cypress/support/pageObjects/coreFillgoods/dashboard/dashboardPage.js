/// <reference types="cypress" />

export class DashboardPage { 
    pageShouldBeDisplayed(){
        cy.wait(10000);
        cy.url().should('include', 'dashboard');
    }


}

export const onDashboardPage = new DashboardPage;