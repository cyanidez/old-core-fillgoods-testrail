/// <reference types="cypress" />

export class MerchantManagementPage {

    clickAddShop(){
        cy.wait(5000);
        cy.contains('button', 'เพิ่มร้านค้าใหม่').click();
    }
    clickEditShop(){
        cy.wait(3000);
        cy.get('tbody > tr:nth-child(2) > td:nth-child(4) > .btn-edit-shop').click();
    }
    clickDeleteShop(){
        cy.wait(3000);
        cy.get('tbody > tr:nth-child(2) > td:nth-child(4) > .btn-disabled-shop').click();
    }
    clickRestoreShop(){
        cy.wait(3000);
        cy.get('tbody > tr:nth-child(2) > td:nth-child(4) > .btn-restore-shop').click();
    }
    clickConfirmBtn(){
        cy.contains('button', 'ยืนยัน').click();
    }
    clickPopupConfirmBtn(){
        cy.get('.v-dialog--active').contains('button', 'ตกลง').click();
    }
    clickUploadSlip(isClick){
        cy.contains('div', 'บังคับอัพโหลดสลิป').find('.v-input--selection-controls__ripple').click({ force: isClick });
    }
    clickOpenAgent(isClick){
        cy.contains('div', 'เปิดใช้ระบบตัวแทน').find('.v-input--selection-controls__ripple').click({ force: isClick });
    }
    clickDisplayAddress(isClick){
        cy.contains('div', 'แสดงที่อยู่ร้านค้าบนใบปะหน้า').find('.v-input--selection-controls__ripple').click({ force: isClick });
    }
    clickPaymentType(paymentType){
        cy.contains('button', paymentType).click();
    }

    fillShopName(shopName){
        cy.get('form > div:nth-child(1) > div > div:nth-child(1) > div > input').clear().type(shopName);
    }
    fillMobileNo(mobileNo){
        cy.get('form > div:nth-child(3) > div > div:nth-child(1) > div > input').clear().type(mobileNo);
    }
    fillEmail(email){
        cy.get('form > div:nth-child(4) > div > div:nth-child(1) > div > input').clear().type(email);
    }
    fillFacebook(fb){
        cy.get('form > div:nth-child(5) > div > div:nth-child(1) > div > input').clear().type(fb);
    }
    fillIg(ig){
        cy.get('form > div:nth-child(6) > div > div:nth-child(1) > div > input').clear().type(ig);
    }
    fillLine(line){
        cy.get('form > div:nth-child(7) > div > div:nth-child(1) > div > input').clear().type(line);
    }
    fillAddress(addr){
        cy.get('form > div:nth-child(8) > div > div:nth-child(1) > div > input').clear().type(addr);
    }

    selectProductType(productType){
        cy.get('.v-select__selections').click();
        cy.get('.v-list').find('.v-list__tile').contains(productType).click();
    }
    selectProvince(province){
        cy.get('form > div:nth-child(9) > form > div:nth-child(1)').click();
        cy.get('.v-list').find('.v-list__tile__title').contains(province).click();
    }
    selectDistrict(district){
        //อำเภอ-เขต
        cy.get('form > div:nth-child(9) > form > div:nth-child(2)').click();
        cy.get('.v-list').find('.v-list__tile__title').contains(district).click();
    }
    selectSubDistrict(subDistrict){
        //ตำบล-แขวง
        cy.get('form > div:nth-child(9) > form > div:nth-child(3)').click();
        cy.get('.v-list').find('.v-list__tile__title').contains(subDistrict).click();
    }
    selectPostCode(postCode){
        cy.get('form > div:nth-child(9) > form > div:nth-child(4)').click();
        cy.get('.v-list').find('.v-list__tile__title').contains(postCode).click();
    }
}

export const onMerchantManagementPage = new MerchantManagementPage