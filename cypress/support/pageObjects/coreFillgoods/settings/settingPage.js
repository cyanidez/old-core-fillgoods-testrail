/// <reference types="cypress" />

export class SettingPage {

    clickSetting(title){
        cy.get('.v-toolbar__content > div:nth-child(7)').click();
        cy.get('.v-list').find('.v-list__tile__title').contains(title).click();
    }
}

export const onSettingPage = new SettingPage