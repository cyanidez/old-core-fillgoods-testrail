/// <reference types="cypress" />

const pageLocator = {
    chkAcceptConsent: '[data-testid="chkAcceptConsent"]'
}

export class ConsentPage { 
    tickAcceptConsent(isTrue = true){
        if (isTrue == true) { 
            cy.get(pageLocator.chkAcceptConsent).check({force:true});
        } else { 
            cy.get(pageLocator.chkAcceptConsent).uncheck({force:true});
        }
    }
}

export const onConsentPage = new ConsentPage; 