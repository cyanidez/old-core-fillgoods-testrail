Feature: Register successfully

    The customer can register to new user successfullly

  Scenario: User register new account with "valid" OTP and configured fixed OTP is "ON" and configure bypass captcha is "ON" 
    Given I clear data with email "automation00001@gmail.com"
    And I open core fillgoods page 
    When I click register tab
    # Then I don't see captcha
    When I input first name as "QAtest"
    And I input last name as "QALastname"
    
    When I input phone number as "077777777"
    Then I see phone number display error message as "กรุณากรอกเบอร์โทรศัพท์ให้ครบตามจำนวน"

    When I input email as "preme.w@fil" 
    Then I see email display error message as "กรุณากรอกรูปแบบอีเมลให้ถูกต้อง"

    When I input password as "12345"
    Then I see password display error as "กรุณากรอกรหัสมากกว่า 6 ตัว"

    When I input phone number as "0777777777"
    And I input email as "preme.w@fillgoods.co"

    And I input password as "123456"
    When I click sign up button
    Then I see login display error as "อีเมลนี้ถูกใช้ไปแล้ว"


    When I input email as "automation00001@gmail.com"
    And I click sign up button 
    Then I see verify otp page is displayed
    When I input otp as "123456"
    And I click ตกลง button
    Then I see survey page is displayed

    When I choose sell channel as "Facebook"
    And I click "ต่อไป" button
    And I choose sales order as "10 - 20 ออเดอร์"
    And I click "ต่อไป" button
    Then I see privacy policy page displayed

    When I accept privacy policy
    And I click "ตกลง" button
    #Then I see dashboard page

    
    # When I input otp as "123456"
    # Then I see ตกลง button is enabled 
    # When I click ตกลง button 
    # Then I registered successfully and the page should be redirect to dashboard  

  # Scenario: User register new account with "invalid" OTP and configure bypass captcha is "ON"
  #   Given I open core fillgoods page 
    # When I click ลงทะเบียน tab
    # Then I don't see captcha
    # When I input first name as "QAtest"
    # And I input last name as "QALastname"
    # And I Input email as "preme.w@fillgoods.co"
    # And I input mobile no as "0777777777"
    # And I input password as "123456"
    # When I click sign up button 
    # Then Page should be redirect to verify otp  
    # When I input otp as "666666"
    # Then I see ตกลง button is enabled 
    # When I click ตกลง button 
    # Then I see error message as "OTP ไม่ถูกต้อง"

# Scenario: User register new account with "empty" OTP and configured fixed OTP is "ON"
    # Given I open core fillgoods page 
    # When I click ลงทะเบียน tab
    # Then I don't see captcha
    # When I input first name as "QAtest"
    # And I input last name as "QALastname"
    # And I Input email as "preme.w@fillgoods.co"
    # And I input mobile no as "0777777777"
    # And I input password as "123456"
    # When I click sign up button 
    # Then Page should be redirect to verify otp  
    # When I don't input OTP value
    # Then I see ตกลง button is enabled 
    # When I click ตกลง button 
    # Then I see error message as "กรุณากรอก OTP ให้ครบตามจำนวน"


