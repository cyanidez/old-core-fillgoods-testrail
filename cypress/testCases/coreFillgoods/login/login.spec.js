/// <reference types="cypress" />

const { onDashboardPage } = require("../../../support/pageObjects/coreFillgoods/dashboard/dashboardPage");
const { onLoginpage } = require("../../../support/pageObjects/coreFillgoods/login/loginPage");
const { onBeamerPage } = require("../../../support/pageObjects/modal/beamerPage");
const { onTutorialPage } = require("../../../support/pageObjects/modal/tutorialPage");

describe('Login', () => {
    let user; 
    let pass; 
    beforeEach(() =>{
        cy.openCoreFillgoodsOldDesign();
        cy.fixture('testData/login.json').then(loginData => {
            user = loginData.validCredential.user; 
            pass = loginData.validCredential.pass; 
        })
    })
    it('Login first time never create product / order / and not be permanant user', () => {
        
        onLoginpage.fillEmail(user);
        onLoginpage.fillPassword(pass);
        onLoginpage.clickLoginButton();
        onDashboardPage.pageShouldBeDisplayed()
        // onTutorialPage.tutorialDisplayed();
        onBeamerPage.closeBeamer();
        onTutorialPage.tutorialShouldBeDisplayed();
        onTutorialPage.closeTutorial();
        
    })

    const getIframeDocument = () => {
        return cy
        .get('#beamerNews')
        // Cypress yields jQuery element, which has the real
        // DOM element under property "0".
        // From the real DOM iframe element we can get
        // the "document" element, it is stored in "contentDocument" property
        // Cypress "its" command can access deep properties using dot notation
        // https://on.cypress.io/its
        .its('0.contentDocument').should('exist')
    }

    const getIframeBody = () => {
        // get the document
        const iframeDocument = getIframeDocument()

        cy.log("iframeDocument = " + iframeDocument);
        // automatically retries until body is loaded
        return iframeDocument.its('body').should('not.be.undefined')
        // wraps "body" DOM element to allow
        // chaining more Cypress commands, like ".find(...)"
        // .then(cy.wrap)  
        
        // return iframeDocument;
    }

})


