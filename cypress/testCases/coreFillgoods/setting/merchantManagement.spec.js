/// <reference types="cypress" />
import {onSettingPage} from '../../support/pageObjects/settings/settingPage';
import {onMerchantManagementPage} from '../../support/pageObjects/settings/merchantManagementPage';
import {onTutorialPage} from "../../support/pageObjects/modal/tutorialPage";

describe('Setting > Merchant Management', () => {
    let title;
    let createShop;
    let editShop;
    let uniqtime = Date.now();

    before('Login to core fillgoods', () => {
        cy.loginToCoreFillgoods();
        cy.fixture('testData/setting/merchant.json').then(data => {
            title = data.title;
            createShop = data.createShop[0];
            editShop = data.editShop[0];

            createShop.shopName += uniqtime;
            editShop.shopName += uniqtime;

            onSettingPage.clickSetting(title);
        })
    })

    it('[Merchant Management] The owner create new merchant', () => {
        onMerchantManagementPage.clickAddShop();
        onMerchantManagementPage.fillShopName(createShop.shopName);
        onMerchantManagementPage.selectProductType(createShop.prototype);
        onMerchantManagementPage.fillMobileNo(createShop.mobileNo);
        onMerchantManagementPage.fillEmail(createShop.email);
        onMerchantManagementPage.fillFacebook(createShop.fb);
        onMerchantManagementPage.fillIg(createShop.ig);
        onMerchantManagementPage.fillLine(createShop.line);
        onMerchantManagementPage.fillAddress(createShop.address);
        onMerchantManagementPage.selectProvince(createShop.province);
        onMerchantManagementPage.selectDistrict(createShop.district);
        onMerchantManagementPage.selectSubDistrict(createShop.subDistrict);
        onMerchantManagementPage.selectPostCode(createShop.postcode);
        onMerchantManagementPage.clickUploadSlip(true);
        onMerchantManagementPage.clickOpenAgent(true);
        onMerchantManagementPage.clickDisplayAddress(true);
        onMerchantManagementPage.clickPaymentType(createShop.paymentType);
        onMerchantManagementPage.clickConfirmBtn();
    })

    it.skip('[Merchant Management] The owner update merchant', () => {
        cy.wait(20000);
        onMerchantManagementPage.clickEditShop();
        onMerchantManagementPage.fillShopName(editShop.shopName);
        onMerchantManagementPage.selectProductType(editShop.prototype);
        onMerchantManagementPage.fillMobileNo(editShop.mobileNo);
        onMerchantManagementPage.fillEmail(editShop.email);
        onMerchantManagementPage.fillFacebook(editShop.fb);
        onMerchantManagementPage.fillIg(editShop.ig);
        onMerchantManagementPage.fillLine(editShop.line);
        onMerchantManagementPage.fillAddress(editShop.address);
        onMerchantManagementPage.selectProvince(editShop.province);
        onMerchantManagementPage.selectDistrict(editShop.district);
        onMerchantManagementPage.selectSubDistrict(editShop.subdistrict);
        onMerchantManagementPage.selectPostCode(editShop.postcode);
        onMerchantManagementPage.clickUploadSlip(true);
        onMerchantManagementPage.clickOpenAgent(true);
        onMerchantManagementPage.clickDisplayAddress(true);
        onMerchantManagementPage.clickPaymentType(editShop.paymentType);
        onMerchantManagementPage.clickConfirmBtn();
    })

    it('[Merchant Management] The owner delete a merchant & rollback a merchant', () => {
        onMerchantManagementPage.clickDeleteShop();
        onMerchantManagementPage.clickPopupConfirmBtn();
        cy.wait(20000);
        onTutorialPage.closeTutorial();

        onMerchantManagementPage.clickRestoreShop();
        cy.wait(20000);
        onTutorialPage.closeTutorial();
    })

})