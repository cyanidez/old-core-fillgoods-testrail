/// <reference types="cypress" />

const { onRegisterPage } = require("../../../support/pageObjects/coreFillgoods/login/registerPage");
const { onLoginpage } = require("../../../support/pageObjects/coreFillgoods/login/loginPage");
const { mUsers } = require("../../../support/models/usersModel");
const { onVerifyOtpPage } = require("../../../support/pageObjects/coreFillgoods/login/verifyOtpPage");
const { onSurveyPage } = require("../../../support/pageObjects/coreFillgoods/login/surveyPage");
const { onPrivacyPolicyPage } = require("../../../support/pageObjects/coreFillgoods/login/privacyPolicyPage");
const { onConsentPage } = require("../../../support/pageObjects/coreFillgoods/consent/consentPage");
const { onPricePlanPage } = require("../../../support/pageObjects/coreFillgoods/pricePlan/pricePlanPage");


describe('Register new account', () => {
    beforeEach(() => {
        mUsers.deleteUserByEmail('automation00001@gmail.com');
        cy.openCoreFillgoods();
    })

    it(`User register new account with "valid" OTP and configured fixed OTP is "ON" and configure bypass captcha is "ON" `, () => {
        onLoginpage.clickTab('สมัครสมาชิก')
        
        onConsentPage.tickAcceptConsent(); 


        onRegisterPage.fillFirstName('QATest');
        onRegisterPage.fillLastName('QALastName');
        onRegisterPage.fillPhoneNumber('077777777');
        onRegisterPage.phoneNumberDisplayError('กรุณากรอกเบอร์โทรศัพท์ให้ครบตามจำนวน');
        onRegisterPage.fillEmail('automation00001@gmail');
        onRegisterPage.emailDisplayError('กรุณากรอกรูปแบบอีเมลให้ถูกต้อง');
        onRegisterPage.fillPassword('12345');
        onRegisterPage.passwordDisplayError('กรุณากรอกรหัสมากกว่า 6 ตัว');

        onRegisterPage.fillPhoneNumber('0777777777');
        onRegisterPage.fillEmail('preme.w@fillgoods.co');
        onRegisterPage.fillPassword('123456');
        onRegisterPage.clickSignupButton();
        onRegisterPage.loginDisplayError('อีเมลนี้ถูกใช้ไปแล้ว')

        onRegisterPage.fillEmail('automation00001@gmail.com');
        onRegisterPage.clickSignupButton();
        onVerifyOtpPage.pageShouldBeDisplayed()  
        onVerifyOtpPage.fillOtp('123456');
        onVerifyOtpPage.clickOKButton();
        onSurveyPage.pageShouldBeDisplayed(); 
        onSurveyPage.chooseChannel('Facebook');
        onSurveyPage.clickNextButton('ต่อไป')
        onSurveyPage.chooseSalesOrder('10 - 20 ออเดอร์');
        onSurveyPage.clickNextButton('ต่อไป'); 
        
        
        onPricePlanPage.choosePricePlan('Silver');

        onPrivacyPolicyPage.pageShouldBeDisplayed()
        onPrivacyPolicyPage.tickAcceptPolicy();
        onPrivacyPolicyPage.clickOKButton();
    })
})

/*

Given('I open core fillgoods page', () => {
    cy.openCoreFillgoods()
})

Given('I clear data with email {string}', (email) => {
    mUsers.deleteUserByEmail(email);
})

When('I click register tab', () => {
    onLoginpage.clickTab('สมัครสมาชิก')
    
})

When('I input first name as {string}', (firstName) => {
    onRegisterPage.fillFirstName(firstName);
})
When('I input last name as {string}', (lastName) => {
    onRegisterPage.fillLastName(lastName);
})
When('I input password as {string}', (password) => {
    onRegisterPage.fillPassword(password);
})
Then('I see password display error as {string}', (password) => {
    onRegisterPage.passwordDisplayError(password);
})
When('I input phone number as {string}', (phoneNumber) => {
    onRegisterPage.fillPhoneNumber(phoneNumber);
})
Then('I see phone number display error message as {string}', (errorMessage) => {
    onRegisterPage.phoneNumberDisplayError(errorMessage)
})
When('I input email as {string}', (email) => {
    onRegisterPage.fillEmail(email);
})
Then('I see email display error message as {string}', (errorMessage) => {
    onRegisterPage.emailDisplayError(errorMessage);
})

When('I click sign up button', () => {
    onRegisterPage.clickSignupButton();
})
Then('I see login display error as {string}', (message)=>{
    onRegisterPage.loginDisplayError(message);
})

Then('I see verify otp page is displayed', () => {
    onVerifyOtpPage.pageShouldBeDisplayed()  
})

When('I input otp as {string}', (otpCode) => {
    onVerifyOtpPage.fillOtp(otpCode);
})
When('I click ตกลง button', () => {
    onVerifyOtpPage.clickOKButton();
})

Then('I see survey page is displayed', () => {
    onSurveyPage.pageShouldBeDisplayed()
})

When('I choose sell channel as {string}', (channel) => {
    onSurveyPage.chooseChannel('Facebook');
})

When('I click {string} button', (label) => {
    onSurveyPage.clickNextButton(label)
})

When('I choose sales order as {string}', (salesOrder) => {
    onSurveyPage.chooseSalesOrder(salesOrder);
})
Then('I see privacy policy page displayed', () => {
    onPrivacyPolicyPage.pageShouldBeDisplayed()
})

When('I accept privacy policy', () => {
    onPrivacyPolicyPage.tickAcceptPolicy();
})

*/