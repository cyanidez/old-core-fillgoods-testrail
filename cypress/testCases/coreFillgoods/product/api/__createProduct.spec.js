/// <reference types="cypress" />

import { apiHelper } from "../../../../support/common/apiHelper"

describe('Create Product API', () => {
    let token
    before(() => {
        cy.authenFirebase(); 
        cy.get('@respFirebase').then(responseFirebase => {
            token = responseFirebase.idToken;
        })
        cy.fixture('testData/createProduct.json').as('reqDataCreateProduct');
    })

    it('create product success', () => {
        
        cy.get('@reqDataCreateProduct').then(reqData => {
            const uniqTime = Date.now();
            let reqHeader = reqData.success.headers; 
            reqHeader.Authorization = "Bearer " + token; 

            let reqBody = reqData.success.body;
            reqBody.name = "productName" + uniqTime;
            reqBody.sku = "SKU" + uniqTime; 
            
            reqBody.sub_products[0].name = "subProductName" + uniqTime;
            reqBody.sub_products[0].sku = "subProductSKU" + uniqTime;
            
            apiHelper.setReqUri('/product/new-create');
            apiHelper.setReqMethod('POST');
            apiHelper.setReqHeader(reqHeader); 
            apiHelper.setReqBody(reqBody);
            

            apiHelper.sendRequest('createProduct');

            cy.get('@createProduct').then(response => {
                cy.log(response.body);
            })

        })

        
    })
})