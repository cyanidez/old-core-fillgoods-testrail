/// <reference types="cypress" />

// import { onLoginOmsPage } from "../../../support/pageObjects/oms/login/loginOmsPage";

import { onLoginOmsPage } from "../../../support/pageObjects/oms/login/loginOmsPage";

describe('Login', () => {
    beforeEach(() => {
        
    })
    it('Login successfully', () => {
        cy.openOms(); 
        
        onLoginOmsPage.fillEmail('dev@fillgoods.co');
        onLoginOmsPage.fillPassword('123456');
        onLoginOmsPage.selectMode('uat');
        onLoginOmsPage.clickLoginButton();

    })
});