/// <reference types="cypress" />

// import { mTestRail } from "../../../support/models/testrail";
import { mUsers } from "../../../support/models/usersModel";
import { onSidebarPage } from "../../../support/pageObjects/oms/sidebar/sidebarPage";

const itsName = require('its-name')


describe('User - Lead pool', () => {
    beforeEach(() => {
        
        indexedDB.deleteDatabase('firebaseLocalStorageDb');

        cy.loginOMS('dev@fillgoods.co', '123456', 'uat'); 
    })
    it('User - Lead Pool', function() {
        // const myItsName = itsName(this);
        
        //cy.log('test'); 
        // mTestRail.addCase(myItsName);
        onSidebarPage.expandMenu(); 
        onSidebarPage.clickMenu('Customer'); 
        onSidebarPage.clickMenu('Lead Pool');
        
        
        mUsers.deleteUserByEmail('ddd@ddd.com');
    })
})